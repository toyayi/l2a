﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using IImageHandler;

namespace Laba2
{
    public partial class Form1 : Form
    {
        static public ImageHandler Fire = new Fire(); //обьект класса Fire
        System.Drawing.Imaging.ImageFormat format; //переменная в который хранится формат открытого изображения

        public Form1()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close(); //Закрытие программы
        }

        private void открытьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap img = new Bitmap(openFileDialog1.FileName); //открываем изображение
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage; 
                pictureBox1.Image = img; //добавляем изображение в picturebox
                Fire.Source = img; //закидуем в клас Fire наше изображение
                format = img.RawFormat; //сохраняем формат открытого изображения
            }
        }

        private void сохранитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap img = Fire.Result; //берем результативное изображение с класса Fire
                img.Save(saveFileDialog1.FileName, format); //сохраняем полученное изображение учитывая сохраненный формат
            }
        }

        private void горящииКраяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _Fire();
        }

        private void _Fire()
        {
            try
            {
                Fire.startHandle((double percent) =>
                {
                    progressBar1.Value = (int)percent;
                });
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox2.Image = Fire.Result;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}