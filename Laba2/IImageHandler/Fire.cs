﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace IImageHandler
{
    public class Fire: ImageHandler
    {
        string _HandlerName = "";

        private Bitmap _source;

        public string HandlerName { get { return _HandlerName; } }

        public void init(SortedList<string, object> parameters)
        {

        }

        public Bitmap Source
        {
            set
            {
                _source = value;
            }
        }

        public Bitmap Result
        {
            get
            {
                return _source;
            }
        }

        public void startHandle(ProgressDelegate progress)
        {
            Bitmap result = new Bitmap(_source);

            result = MedianFilter(_source); //Медианный фильтр

            result = Grandient(result); //добавление грандиента

            _source = result;
        }

        //Функци медианного фильтра
        private Bitmap MedianFilter(Bitmap btm)
        {
            Bitmap btmp = new Bitmap(btm);

            int[] mask = new int[9];

            Color c;

            for (int ii = 0; ii < _source.Width; ii++)
            {
                for (int jj = 0; jj < _source.Height; jj++)
                {

                    if (ii - 1 >= 0 && jj - 1 >= 0)
                    {
                        c = _source.GetPixel(ii - 1, jj - 1);
                        mask[0] = c.ToArgb();
                    }
                    else
                    {
                        mask[0] = 0;
                    }

                    if (jj - 1 >= 0 && ii + 1 < _source.Width)
                    {
                        c = _source.GetPixel(ii + 1, jj - 1);
                        mask[1] = c.ToArgb();
                    }
                    else
                        mask[1] = 0;

                    if (jj - 1 >= 0)
                    {
                        c = _source.GetPixel(ii, jj - 1);
                        mask[2] = c.ToArgb();
                    }
                    else
                        mask[2] = 0;

                    if (ii + 1 < _source.Width)
                    {
                        c = _source.GetPixel(ii + 1, jj);
                        mask[3] = c.ToArgb();
                    }
                    else
                        mask[3] = 0;

                    if (ii - 1 >= 0)
                    {
                        c = _source.GetPixel(ii - 1, jj);
                        mask[4] = c.ToArgb();
                    }
                    else
                        mask[4] = 0;

                    if (ii - 1 >= 0 && jj + 1 < _source.Height)
                    {
                        c = _source.GetPixel(ii - 1, jj + 1);
                        mask[5] = c.ToArgb();
                    }
                    else
                        mask[5] = 0;

                    if (jj + 1 < _source.Height)
                    {
                        c = _source.GetPixel(ii, jj + 1);
                        mask[6] = c.ToArgb();
                    }
                    else
                        mask[6] = 0;
                    if (ii + 1 < _source.Width && jj + 1 < _source.Height)
                    {
                        c = _source.GetPixel(ii + 1, jj + 1);
                        mask[7] = c.ToArgb();
                    }
                    else
                        mask[7] = 0;
                    c = _source.GetPixel(ii, jj);
                    mask[8] = c.ToArgb();
                    Array.Sort(mask);
                    int mid = mask[4];
                    btmp.SetPixel(ii, jj, Color.FromArgb(mid));
                }
            }

            return btmp;
        }

        //Функция грандиента
        private Bitmap Grandient(Bitmap btm)
        {
            Bitmap btmp = new Bitmap(btm);

            double t;

            for (int i = 0; i < btm.Width - 1; i++)
            {
                for (int j = 0; j < btm.Height - 1; j++)
                {
                    t = Math.Abs(Math.Sqrt(Math.Pow(btm.GetPixel(i + 1, j).G - btm.GetPixel(i, j).G, 2) + Math.Pow(btm.GetPixel(i, j + 1).G - btm.GetPixel(i, j).G, 2)));
                    if (t >= 5)
                        btmp.SetPixel(i, j, Color.White);
                    //else
                    //    btmp.SetPixel(i, j, Color.Empty);
                }
            }

            return btmp;
        }
    }
}