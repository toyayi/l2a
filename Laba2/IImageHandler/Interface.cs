﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace IImageHandler
{
    public delegate void ProgressDelegate(double percent);

    public interface ImageHandler
    {
        //получение осмысленного имени обработчика
        string HandlerName { get; }

        //Инициализация параметров обработчика
        void init(SortedList<string, object> parameters);

        //Установка изображения-источника
        Bitmap Source { set; }

        //Получение изображения-результата
        Bitmap Result { get; }

        //Запуск обработки
        void startHandle(ProgressDelegate progress);
    }
}
